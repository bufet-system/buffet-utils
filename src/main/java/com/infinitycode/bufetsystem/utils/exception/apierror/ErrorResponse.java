package com.infinitycode.bufetsystem.utils.exception.apierror;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

/**
 * Custom error response
 */

@Data
@AllArgsConstructor
public class ErrorResponse {

  private String errorCode;
  private String description;
  private Set<ErrorDetail> errorDetails;

}