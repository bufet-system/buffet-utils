package com.infinitycode.bufetsystem.utils.exception.apierror;

import lombok.Getter;

@Getter
public class MissingRequestFieldErrorDetail extends ErrorDetail {

  public MissingRequestFieldErrorDetail(String fieldName) {
    this.errorDetailsCode = "REQUEST_FIELD_ERROR";
    this.value = fieldName;
  }
}