package com.infinitycode.bufetsystem.utils.exception.apierror;

import lombok.Getter;

@Getter
public abstract class ErrorDetail {

 protected String errorDetailsCode;
 protected String value;
}