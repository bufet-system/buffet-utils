package com.infinitycode.bufetsystem.utils.exception;

import com.infinitycode.bufetsystem.utils.exception.apierror.ErrorDetail;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.Set;

@Getter
public abstract class AbstractException extends RuntimeException {
  private static final long serialVersionUID = 3370492612356287675L;

  protected HttpStatus httpStatus;
  protected String errorCode;
  protected String description;
  protected Set<ErrorDetail> errorDetails;

  public AbstractException(String message) {
    super(message);
    this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    this.errorCode = "ERROR";
    this.description = "Error occur";
    errorDetails = Collections.emptySet();
  }
}